import React, { useState, useEffect } from 'react';
import axios from 'axios';

const Login = ({ onLogin }) => {
  const signal = axios.CancelToken.source();
  const [roomId, setRoomId] = useState('');
  const [userName, setUserName] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    return () => {
      signal.cancel('Cancel request from useEffect');
    }
  }, []);

  const onChangeInput = (callback) => (evt) => {
    const { value } = evt.target;

    if (value) {
      callback(value);
    }
  };

  const handleSubmit = async (evt) => {
    evt.preventDefault();

    if (!roomId || !userName) {
      alert('Все поля обязательны к заполнению!');
      return;
    }

    const data = {
      roomId,
      userName,
    };

    setIsLoading(true);

    await axios.post(`${process.env.BACKEND_URL}/rooms`, data, {
      cancelToken: signal.token,
    });

    setIsLoading(false);
    onLogin(data);
  };

  return (
    <div className="login">
      <form onSubmit={handleSubmit}>
        <div className="login__field">
          <input className="login__input" type="text" placeholder="Имя комнаты" value={ roomId } onChange={ onChangeInput(setRoomId) } />
        </div>
        <div className="login__field">
          <input className="login__input" type="text" placeholder="Ваше имя" value={ userName } onChange={ onChangeInput(setUserName) } />
        </div>
        <div className="login__field">
          <button className="login__button" disabled={ isLoading }>{ isLoading ? 'Вход...' : 'Войти' }</button>
        </div>
      </form>
    </div>
  )
};

export default Login;
