export default (state, action) => {
  switch (action.type) {
    case 'joined': {
      return {
        ...state,
        ...action.payload,
      }
    }

    case 'set_users': {
      return {
        ...state,
        users: action.payload,
      }
    }

    case 'set_messages': {
      return {
        ...state,
        messages: action.payload,
      }
    }

    case 'set_new_message': {
      return {
        ...state,
        messages: [...state.messages, action.payload],
      }
    }

    default:
      return state;
  }
};
