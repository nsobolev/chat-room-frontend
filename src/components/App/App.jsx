import React, { useEffect, useReducer } from 'react';

import socket from '../../socket';
import reducer from './reducer';

import Login from '../Login';
import Chat from '../Chat';

import '../../styles/bootstrap.less';

const App = () => {
  const [state, dispatch] = useReducer(reducer, {
    roomId: null,
    userName: null,
    users: [],
    messages: [],
    joined: false,
  });

  useEffect(() => {
    socket.on('ROOM:CHANGE_USERS', ({ users, messages }) => {
      dispatch({ type: 'set_users', payload: users });
      dispatch({ type: 'set_messages', payload: messages });
    });

    socket.on('ROOM:CHANGE_MESSAGES', message => {
      dispatch({ type: 'set_new_message', payload: message });
    })
  }, []);

  const onLogin = (data) => {
    dispatch({
      type: 'joined',
      payload: {
        ...data,
        joined: true,
      }
    });

    socket.emit('ROOM:JOIN', data);
  };

  return (
    <div className="wrapper">
      <div className="wrapper__container container">
        <div className="wrapper__login">
          {
            state.joined ?
              <Chat
                users={ state.users }
                messages={ state.messages }
                roomId={ state.roomId }
                userName={ state.userName }
              />
              :
              <Login onLogin={ onLogin } />
          }
        </div>
      </div>
    </div>
  )
};

export default App;
