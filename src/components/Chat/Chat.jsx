import React, { useEffect } from 'react';
import socket from '../../socket';

function Chat({ users = [], messages = [], roomId, userName }) {
  const [messageValue, setMessageValue] = React.useState('');
  const messagesRef = React.useRef(null);

  useEffect(() => {
    const messageBlock = messagesRef.current;
    messageBlock.scroll(0, messageBlock.scrollHeight)
  }, [messages]);

  const handleSubmit = (evt) => {
    evt.preventDefault();

    if (!messageValue) {
      alert('Введите сообщение');
      return;
    }

    const data = {
      roomId,
      userName,
      text: messageValue,
    };

    socket.emit('ROOM:MESSAGES', data);
  };

  return (
    <div className="chat">
      <div className="chat-users">
        Комната: <b>{roomId}</b>
        <hr />
        <b>Онлайн ({ users.length }):</b>
        <ul>
          {
            users.map((user, index) => {
              return <li key={ index }>{ user }</li>
            })
          }
        </ul>
      </div>
      <div className="chat-messages">
        <div ref={messagesRef} className="messages">
          {
            messages.map((message, index) => (
              <div className="message" key={ index }>
                <p>{message.text}</p>
                <div>
                  <span>{message.userName}</span>
                </div>
              </div>
            ))
          }
        </div>
        <form onSubmit={ handleSubmit }>
          <textarea
            value={messageValue}
            onChange={(e) => setMessageValue(e.target.value)}
            className="form-control"
            rows="3"></textarea>
          <button className="btn btn-primary" disabled={Boolean(!messageValue)}>
            Отправить
          </button>
        </form>
      </div>
    </div>
  );
}

export default Chat;
